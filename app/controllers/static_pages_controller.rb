# classes in ruby hold methods, defined using the def keyword.
# angle brackets "<" indicates inheritance  
class StaticPagesController < ApplicationController
# when visiting the  url /static_pages/home, rails looks in the static pages
# controller, and executes the code in home, and then renders then view 
# corresponding to the action.
  def home
  end

  def help
  end
  
  def about
  end
end
